# Download files

Download source code to your PC, unpack folder, open IDE like Visual Studio Code or something simmilar.

# Getting Started with Create React App

In terminal (CTRL + `) type:

npx create-react-app teaCode,
now copy all files from download folder to new teaCode folder then type
cd teaCode,
npm start

Runs the app in the development mode.
Open http://localhost:3000 to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.


